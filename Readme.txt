Candidate task - Vitor Torres

First:
$ git clone git@bitbucket.org:vtorres/open_signal.git
$ cd open_signal

* Task 1 - Equilibrium index
To run this:
$ python equilibrium_index.py

Task 2 - London Towers
I've created this task with Django. I'm limiting the results to 1000 because of performance issues, although I know that, if done properly I should add pagination to the results and handle some of the Google Maps events, like 'dragend' or zoom_changed' to update the markers on the map after some user interaction. To setup and run, execute following commands:
$ cd london_towers
$ virtualenv --no-site-packages env
$ source env/bin/activate
$ pip install -r requirements.txt
$ cd london_towers
$ ./manage.py runserver
and then open a browser on http://127.0.0.1:8000
