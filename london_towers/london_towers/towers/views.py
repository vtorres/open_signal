from django.views.generic.base import TemplateView, View
from django.http.response import HttpResponseBadRequest, HttpResponse
import psycopg2
from london_towers.settings import EXTERNAL_DB_NAME, EXTERNAL_DB_USER,\
    EXTERNAL_DB_PASSWORD, EXTERNAL_DB_HOST
import json

class HomeView(TemplateView):
    template_name = 'home.html'


class SearchView(View):
    
    def get_database_connection(self):
        try:
            conn = psycopg2.connect(
                "dbname='%s' user='%s' host='%s' password='%s'" % \
                (EXTERNAL_DB_NAME, EXTERNAL_DB_USER, EXTERNAL_DB_HOST, EXTERNAL_DB_PASSWORD)
            )
        except:
            print "I am unable to connect to the database"
        return conn
    
    def post(self, request, *args, **kwargs):
        return HttpResponseBadRequest()

    def get(self, request, *args, **kwargs):
        bounding_box = request.GET.get('bouding_box', None)
        network_type = request.GET.get('network_type', None)
        phone_type = request.GET.get('phone_type', None)
        conn = self.get_database_connection()
        cur = conn.cursor()
        query = """SELECT * FROM london_towers"""
        where_clause = []
        if bounding_box:
            where_clause.append("""ST_Within(geom,
                ST_GeomFromText('POLYGON((%s))', 4326))""" % bounding_box)
        if network_type:
            if network_type == '2g':
                where_clause.append('is_2g = True')
            elif network_type == '3g':
                where_clause.append('is_3g = True')
            elif network_type == 'lte':
                where_clause.append(' is_lte = True')
        if phone_type:
            where_clause.append("phone_type = '%s'" % phone_type)
        if where_clause:
            query = '%s WHERE %s' % (query, ' and '.join(where_clause))
        query = '%s limit 1000;' % query
                
        cur.execute(query)
        
        rows = cur.fetchall()
        
        data = json.dumps(rows)
        conn.close()
        return HttpResponse(data, content_type='application/json') 