from django.conf.urls import url
from towers.views import HomeView, SearchView

urlpatterns = [
    url(r'search/$', SearchView.as_view(), name='search'),
    url(r'$', HomeView.as_view(), name='home'),
]