def solution(A, n):
    """
    This function return the equilibrium index of a list,
    if any exist, or -1 if equilibrium index cannot be found.
    """
    if not A:
        # If list is empty, return -1
        return -1
    equilibrium_indexes = []
    for index, element in enumerate(A):
        # for each index we break the list into to parts, left will be
        # a list containing the elements of lower index and right with elements
        # of higher index. Sum will calculate the sum of the values of each list
        left_sum = sum(A[:index])
        right_sum = sum(A[index + 1:])

        if left_sum == right_sum:
            # if left_sum equals right_sum, we return index as an equilibrium index
            return index
    return -1

if __name__ == '__main__':
    print 'Please insert the numbers that comprise your list, separated by space'
    numbers = raw_input()
    A = map(lambda x: int(x), numbers.split())
    print 'Equilibrium index of %s: %s' % (A, solution(A, len(A)))
